`define Xdig0 time_in[3:0]
`define Xdig1 time_in[7:4]
`define Xdig2 time_in[11:8]
`define Xdig3 time_in[15:12]

`define Ydig0 time_adder[3:0]
`define Ydig1 time_adder[7:4]
`define Ydig2 time_adder[11:8]
`define Ydig3 time_adder[15:12]

/*`define OneFiftdig0 add150[3:0]
`define OneFiftdig1 add150[7:4]
`define OneFiftdig2 add150[11:8]
`define OneFiftdig3 add150[15:12]

`define Twodig0 add200[3:0]
`define Twodig1 add200[7:4]
`define Twodig2 add200[11:8]
`define Twodig3 add200[15:12]

`define Fivedig0 add500[3:0]
`define Fivedig1 add500[7:4]
`define Fivedig2 add500[11:8]
`define Fivedig3 add500[15:12]
*/

`define Zdig0 Zint[3:0]
`define Zdig1 Zint[7:4]
`define Zdig2 Zint[11:8]
`define Zdig3 Zint[15:12]
`define Zdig4 Zint[19:16]


module BCDAdder (time_in, time_adder, time_out); //bcd is different than binary because each digits place has indivual binary encoding for the tens and hundreds
input [15:0] time_in; //take in 4 button parameters 

//input [15:0] add50; //set it 50
//input [15:0] add150; //set it 150
//input [15:0] add200; //set it 200
//input [15:0] add250; //set it 250 each of the ones above will be set in BCD format

input[15:0] time_adder;

output [15:0] time_out; //since addition can blow over to 5 bits

wire[4:0] S0;
wire[4:0] S1;
wire[4:0] S2;
wire[4:0] S3;
wire[19:0] Zint;

wire C0;
wire C1;
wire C2;
wire C3;

/*intital 
begin 
add50 = 16'b0000000001010000;
add150 = 16'b0000000101010000;
add250 = 16'b0000001001010000;
add500 = 16'b0000010100000000;
end*/

assign S0 = `Xdig0 + `Ydig0;
assign `Zdig0 = (S0 > 9) ? S0[3:0] + 6 : S0[3:0];
assign C0 = (S0>9) ? 1'b1 : 1'b0;

assign S1 = `Xdig1 + `Ydig1 + C0;
assign `Zdig1 = (S1 > 9) ? S1[3:0] + 6 : S1[3:0];
assign C1 = (S1 > 9) ? 1'b1 : 1'b0;

assign S2 = `Xdig2 + `Ydig2 + C1;
assign `Zdig2 = (S2 > 9) ? S2[3:0] + 6 : S2[3:0];
assign C2 = (S0 > 9) ? 1'b1 : 1'b0;

assign S3 = `Xdig3 + `Ydig3 + C2;
assign `Zdig3 = (S3 > 9) ? S3[3:0] + 6 : S3[3:0];
assign  `Zdig4 = (S3 > 9) ? 1'b1 : 1'b0;
assign time_left = (`Zdig4 > 1'b0) ? 20'b1001100110011001 : Zint[15:0]; //9999 in BCD format


endmodule

