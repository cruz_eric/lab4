module BinarytoBCD(clk, time_left, thousands, hundreds, tens, ones);
input clk;
input[15:0] time_left;
output[3:0] thousands;
output[3:0] hundreds;
output[3:0] tens;
output[3:0] ones;

//reg[15:0] Time_Left;
reg[3:0] Thousands;
reg[3:0] Hundreds;
reg[3:0] Tens;
reg[3:0] Ones;

integer i;

initial
begin
	Thousands = 0;
	Hundreds = 0;
	Tens = 0;
	Ones = 0;
end

always @(posedge clk)
begin                             //whenever there is a new input
	Thousands = 0;
	Hundreds = 0;
	Tens = 0;
	Ones = 0;
	for(i = 15; i >= 0; i = i-1)
	begin
		if(Thousands >= 5) Thousands = Thousands + 3;
		if(Hundreds >= 5) Hundreds = Hundreds + 3;
		if(Tens >= 5) Tens = Tens + 3;
		if(Ones >= 5) Ones = Ones + 3;
		Thousands = Thousands << 1;
		Thousands[0] = Hundreds[3];
		Hundreds = Hundreds << 1;
		Hundreds[0] = Tens[3];
		Tens = Tens << 1;
		Tens[0] = Ones[3];
		Ones = Ones << 1;
		Ones[0] = time_left[i];
	 end
end

assign thousands = Thousands;
assign hundreds = Hundreds;
assign tens = Tens;
assign ones = Ones;
endmodule