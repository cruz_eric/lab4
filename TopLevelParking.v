<<<<<<< HEAD
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/09/2018 03:07:58 PM
// Design Name: 
// Module Name: TopLevelParking
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TopLevelParking(add50, add150, add200, add500, reset10, reset205, clk, ConcatAll);
input add50, add150, add200, add500, reset10, reset205, clk;
//output wire [6:0]seven_seg;
//output wire [3:0]anode_out;
output ConcatAll;
wire a4hzClk;
wire a250hzClk;

wire add50SP;
wire add150SP;
wire add200SP;
wire add500SP;

wire [15:0]time_left;
wire [2:0]anode;

wire [3:0]Thousands;
wire [3:0]Hundreds;
wire [3:0]Tens;
wire [3:0]Ones;
wire [15:0] ConcatAll;

assign ConcatAll = {Thousands, Hundreds, Tens, Ones};

complexDivider C1(10, clk, a4hzClk); //25000000
complexDivider C3(2, clk, a250hzClk);	//400000

debounce S1(add50, clk, add50SP);
debounce S2(add150, clk, add150SP);
debounce S3(add200, clk, add200SP);
debounce S4(add500, clk, add500SP);

controller B1(add50SP, add150SP, add200SP, add500SP, reset10, reset205, a4hzClk, a250hzClk, time_left, anode);

BinarytoBCD Q1(clk, time_left, Thousands, Hundreds, Tens, Ones);
//bcd_seven D1(ConcatAll, anode, seven_seg, anode_out); Commented out for debugging purposes


endmodule

=======
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/09/2018 03:07:58 PM
// Design Name: 
// Module Name: TopLevelParking
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TopLevelParking(add50, add150, add200, add500, reset10, reset205, clk, seven_seg, anode_out);
input add50, add150, add200, add500, reset10, reset205, clk;
output wire [6:0]seven_seg;
output wire [3:0]anode_out;

wire a4hzClk;
wire a250hzClk;

wire add50SP;
wire add150SP;
wire add200SP;
wire add500SP;

wire [15:0]time_left;
wire [2:0]anode;

wire [3:0]Thousands;
wire [3:0]Hundreds;
wire [3:0]Tens;
wire [3:0]Ones;
wire [15:0] ConcatAll;

assign ConcatAll = {Thousands, Hundreds, Tens, Ones};

complexDivider C1(10, clk, a4hzClk); //25000000
complexDivider C3(2, clk, a250hzClk);	//400000

debounce S1(add50, clk, add50SP);
debounce S2(add150, clk, add150SP);
debounce S3(add200, clk, add200SP);
debounce S4(add500, clk, add500SP);

controller B1(add50SP, add150SP, add200SP, add500SP, reset10, reset205, a4hzClk, a250hzClk, time_left, anode);

BinarytoBCD Q1(clk, time_left, Thousands, Hundreds, Tens, Ones);
bcd_seven D1(ConcatAll, anode, seven_seg, anode_out);


endmodule

>>>>>>> d3fff3fdc3aeb9c92715e25bc35450b81f8b1112
