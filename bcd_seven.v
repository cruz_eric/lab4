module bcd_seven (time_left, anode, seven, AN_on);
 input [15:0] time_left;
 input [2:0] anode;

 output[7:1] seven;
 output[3:0] AN_on;
 reg [7:1] seven; //MSB will be indicating which anode to turn on 
 reg [3:0] AN_on;

 always @(time_left or anode) 
 begin
 	case (anode)
	4'b0000 : begin //turn on anode 0 (ones)
			case(time_left[3:0])
 				4'b0000 : begin seven = 7'b1000000; AN_on = 4'b1110; end //digit 0
 				4'b0001 : begin seven = 7'b1111001 ; AN_on = 4'b1110; end//digit 1
 				4'b0010 : begin seven = 7'b0100100 ; AN_on = 4'b1110; end //digit 2
 				4'b0011 : begin seven = 7'b0110000 ; AN_on = 4'b1110; end //digit 3
 				4'b0100 : begin seven = 7'b0011001 ; AN_on = 4'b1110; end//digit 4
 				4'b0101 : begin seven = 7'b0010010 ; AN_on = 4'b1110;end //digit 5
 				4'b0110 : begin seven = 7'b0000010 ; AN_on = 4'b1110; end//digit 6
 				4'b0111 : begin seven = 7'b1111000 ; AN_on = 4'b1110;end //digit 7
 				4'b1000 : begin seven = 7'b0000000 ; AN_on = 4'b1110; end//digit 8
 				4'b1001 : begin seven = 7'b0010000 ; AN_on = 4'b1110;end //digit 9
 				default : begin seven = 7'b1111111 ; AN_on = 4'b1110; end //nothing on the board
 			endcase 
		   end
	4'b0001 : begin  // turn on anode 1 (tens)
			case(time_left[7:4])
 				4'b0000 : begin seven = 7'b1000000 ; AN_on = 4'b1101; end //digit 0
 				4'b0001 : begin seven = 7'b1111001 ; AN_on = 4'b1101; end//digit 1
 				4'b0010 : begin seven = 7'b0100100 ; AN_on = 4'b1101; end//digit 2
 				4'b0011 : begin seven = 7'b0110000 ; AN_on = 4'b1101; end//digit 3
 				4'b0100 : begin seven = 7'b0011001 ; AN_on = 4'b1101; end //digit 4
 				4'b0101 : begin seven = 7'b0010010 ; AN_on = 4'b1101; end//digit 5
 				4'b0110 : begin seven = 7'b0000010 ; AN_on = 4'b1101; end//digit 6
 				4'b0111 : begin seven = 7'b1111000 ; AN_on = 4'b1101; end//digit 7
 				4'b1000 : begin seven = 7'b0000000 ; AN_on = 4'b1101; end//digit 8
 				4'b1001 : begin seven = 7'b0010000 ; AN_on = 4'b1101; end//digit 9
 				default : begin seven = 7'b1111111; AN_on = 4'b1101; end //nothing on the board
 			endcase
		   end 
	4'b0010 : begin AN_on = 4'b0100; // turn on anode 2; (hundreds)
			case(time_left[11:8])
 				4'b0000 : begin seven = 7'b1000000 ; AN_on = 4'b1011; end //digit 0
 				4'b0001 : begin seven = 7'b1111001 ; AN_on = 4'b1011; end//digit 1
 				4'b0010 : begin seven = 7'b0100100 ; AN_on = 4'b1011; end//digit 2
 				4'b0011 : begin seven = 7'b0110000 ; AN_on = 4'b1011; end//digit 3
 				4'b0100 : begin seven = 7'b0011001 ; AN_on = 4'b1011; end//digit 4
 				4'b0101 : begin seven = 7'b0010010 ; AN_on = 4'b1011; end//digit 5
 				4'b0110 : begin seven = 7'b0000010 ; AN_on = 4'b1011; end//digit 6
 				4'b0111 : begin seven = 7'b1111000 ; AN_on = 4'b1011; end//digit 7
 				4'b1000 : begin seven = 7'b0000000 ; AN_on = 4'b1011; end//digit 8
 				4'b1001 : begin seven = 7'b0010000 ; AN_on = 4'b1011; end//digit 9
 				default : begin seven = 7'b1111111 ; AN_on = 4'b1011; end//nothing on the board
 			endcase
		  end 
	4'b0011 : begin AN_on = 4'b1000; // turn on anode 3 (thousands)
			case(time_left[15:12])
 				4'b0000 : begin seven = 7'b1000000 ; AN_on = 4'b0111; end//digit 0
 				4'b0001 : begin seven = 7'b1111001 ; AN_on = 4'b0111; end//digit 1
 				4'b0010 : begin seven = 7'b0100100 ; AN_on = 4'b0111; end//digit 2
 				4'b0011 : begin seven = 7'b0110000 ; AN_on = 4'b0111; end//digit 3
 				4'b0100 : begin seven = 7'b0011001 ; AN_on = 4'b0111; end//digit 4
 				4'b0101 : begin seven = 7'b0010010 ; AN_on = 4'b0111; end//digit 5
 				4'b0110 : begin seven = 7'b0000010 ; AN_on = 4'b0111; end//digit 6
 				4'b0111 : begin seven = 7'b1111000 ; AN_on = 4'b0111; end//digit 7
 				4'b1000 : begin seven = 7'b0000000 ; AN_on = 4'b0111; end//digit 8
 				4'b1001 : begin seven = 7'b0010000 ; AN_on = 4'b0111; end//digit 9
 				default : begin seven = 7'b1111111 ; AN_on = 4'b0111; end//nothing on the board
 			endcase 
		  end
	default:
	begin
		case(time_left[15:12])
 				4'b0000 : begin seven = 7'b1000000 ; AN_on = 4'b1111; end//digit 0
 				4'b0001 : begin seven = 7'b1111001 ; AN_on = 4'b1111; end//digit 1
 				4'b0010 : begin seven = 7'b0100100 ; AN_on = 4'b1111; end//digit 2
 				4'b0011 : begin seven = 7'b0110000 ; AN_on = 4'b1111; end//digit 3
 				4'b0100 : begin seven = 7'b0011001 ; AN_on = 4'b1111; end//digit 4
 				4'b0101 : begin seven = 7'b0010010 ; AN_on = 4'b1111; end//digit 5
 				4'b0110 : begin seven = 7'b0000010 ; AN_on = 4'b1111; end//digit 6
 				4'b0111 : begin seven = 7'b1111000 ; AN_on = 4'b1111; end//digit 7
 				4'b1000 : begin seven = 7'b0000000 ; AN_on = 4'b1111; end//digit 8
 				4'b1001 : begin seven = 7'b0010000 ; AN_on = 4'b1111; end//digit 9
 				default : begin seven = 7'b1111111 ; AN_on = 4'b1111; end//nothing on the board
 		endcase
	end

	endcase
 end 
 endmodule

