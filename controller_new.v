module complexDivider(count, clk100Mhz, slowClk);
  input [27:0]count;		//used to determine frequency
  input clk100Mhz; //fast clock
  output reg slowClk; //slow clock

  reg[27:0] counter;

  initial begin
    counter = 0;
    slowClk = 0;
  end

  always @ (posedge clk100Mhz)
  begin
    if(counter == count) 
    begin
      counter <= 1;
      slowClk <= ~slowClk;
    end
    else 
    begin
      counter <= counter + 1;
    end
  end

endmodule



module controller(add50SP, add150SP, add200SP, add500SP, reset10, reset205 ,a4hzClk, a62hzClk, time_left, anode_out);		
input add50SP, add150SP, add200SP, add500SP, reset10, reset205, a62hzClk, a4hzClk;
output [15:0]time_left;
output [2:0] anode_out;

reg [15:0]time_left;			//logbase2 of 9999 = 13.2
wire add50SP, add150SP, add200SP, add500SP;
wire a4hzClk;		//for operation
wire a62hzClk; //for 7segment display

wire[3:0] Thousands; 
wire[3:0] Hundreds;
wire[3:0] Tens;
wire[3:0] Ones;

//reg[15:0] time_left_sat; //What will be sending into the BinarytoBCD Counter
wire flash200;
wire flash0;
reg add50, add150, add200, add500;


reg [1:0]count_decrement;
reg [2:0]count_flash200;
reg [1:0]count_flash0;
reg [5:0] button_counter; //indicates which light to show
reg [2:0] anode; //AN0, AN1, AN2, AN3
reg[2:0] anode_out;
reg[6:0] seven_seg;



assign flash0 = (time_left == 0) ? 1 : 0;
assign flash200 = (flash0) ? 0 : ((time_left < 200) ? 1 : 0);



initial
begin
	count_decrement = 0;
	button_counter = 0;
	count_flash200 = 0;
	count_flash0 = 0;
	time_left = 0;
	add50 = 0;
	add150 = 0;
	add200 = 0;
	add500 = 0;
	anode = 0;
end


always@(posedge a62hzClk) //will increment which node is to be chosen
begin
/*
	if(button_counter > 0 && button_counter < 5) begin button_counter = button_counter + 1; anode = 0; end //turn on AN0
	else if(button_counter > 4 && button_counter < 9) begin button_counter = button_counter + 1; anode = 1; end //turn on AN1
	else if(button_counter > 8 && button_counter < 13) begin button_counter = button_counter + 1; anode = 2; end  //turn on AN2
	else if(button_counter > 12 && button_counter < 16) begin button_counter = button_counter + 1; anode = 3; end //turn on AN3
	else begin button_counter = 1; anode = 0; end //last case reverts back to beginning
	*/
	if(anode == 0) anode = 1;
	else if(anode == 1) anode = 2;
	else if(anode == 2) anode = 3;
	else if(anode == 3) anode = 0;
	else anode = 0;

	anode_out = anode;

	if((count_flash200 >= 2) && flash200) anode_out = 4;
	if((count_flash0 >= 4) && flash0) anode_out = 4;
end

always@*	//We're gonna use this since we now have a BinarytoBCD converter so this is much easier
begin
	if(add50SP) time_left = time_left + 50;
	else if(add150SP) time_left = time_left +  150;
	else if(add200SP) time_left = time_left +  200;
	else if(add500SP) time_left = time_left + 500;
	else if(count_decrement == 3) time_left = time_left-1;
	else begin
	end

	if(reset10) time_left = 10;
	else if(reset205) time_left = 205;
	else begin
	end

	if(time_left > 9999) time_left = 9999;
	else begin
	end

end

always@(posedge a4hzClk)
begin
	//decrementer
	if(time_left > 0)
	begin
		if(count_decrement == 3) 
		begin
			count_decrement = 0;
		end
		else
		begin
			count_decrement = count_decrement + 1;
		end
	end
	else count_decrement = 0;

	//flasher
	if(flash200)
	begin
		if(count_flash200 == 7)
		begin
			count_flash200=0;
		end
		else 
		begin
			count_flash200 = count_flash200 + 1;
		end
	end
	else 
	begin
		count_flash200 = 0;
	end

	if(flash0)
	begin
		if(count_flash0 == 3)
			begin
				count_flash0=0;
			end
		else 
			begin
				count_flash0 = count_flash0 + 1;
			end
	end
	else 
	begin
		count_flash0 = 0;
	end
end

/*
always@(posedge add500SP, posedge add200SP, posedge add150SP, posedge add50SP)
begin
	if(add50SP) add50
end*/
endmodule

