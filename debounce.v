module DFF(D, CLK, Q, QN);
input D;
input CLK;
output Q;
output QN;

reg Q;
reg QN;
initial
begin
  Q = 1'b0;
  QN = 1'b1;
end

always @(negedge CLK)
begin
  Q <= D;
  QN <= ~D;
end

endmodule

module AND2(A1, A2, Z);
input A1, A2;
output Z;

assign Z = A1 & A2;

endmodule

module debounce(in, clk, out);
input in;
input clk;
output out;

wire Qa;
wire QaN;
wire Qb;
wire QbN;
wire S1;
wire S1N;
wire out;
wire outN;

DFF D1(in, clk, Qa, QaN);
DFF D2(Qa, clk, Qb, QbN);
DFF D3(Qb, clk, S1, S1N);
AND2 A1(Qb, S1N, out);

endmodule